﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Elements
{
    public class PromoHeader : Element
    {

        public Button StartButton { get; }


        public PromoHeader(IWebDriver driver, By locator) : base(driver, locator)
        {
            StartButton = new Button(driver, By.XPath("/html/body/div/header/div/div[1]/div[2]/a[2]"));
        }

        
        
    }
}
