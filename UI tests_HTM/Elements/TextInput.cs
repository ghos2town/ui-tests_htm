﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Elements
{
    public class TextInput : Element
    {

        public TextInput(IWebDriver driver, By locator) : base(driver, locator)
        {

        }

        public void Clear()
        {
            _driver.FindElement(_locator).Clear();
        }

        public void EnterText(string text)
        {
            _driver.FindElement(_locator).SendKeys(text);
        }
    }
}
