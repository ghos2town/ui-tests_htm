﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Elements
{
    public class Header : Element
    {    

        public Dropdown LanguageDropdown { get; }
        public Button EnterButton { get; }

        public Header(IWebDriver driver, By locator) : base(driver, locator)
        {
            LanguageDropdown = new Dropdown(driver, By.XPath("/html/body/div[1]/main/header/div/div/div[1]/div/div/button"));
            EnterButton = new Button(driver, By.XPath("/html/body/div[1]/main/header/div/div/div[3]/div/a[2]"));
        }

       
 
    }
}
