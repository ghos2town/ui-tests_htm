﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Elements
{
    public class Element
    {
        public IWebDriver _driver;
        public By _locator;

        public Element(IWebDriver driver, By locator)
        {
            _driver = driver;
            _locator = locator;
        }

        public void Click()
        {
            _driver.FindElement(_locator).Click();
        }


        public IWebElement _element;

        public Element(IWebElement element)
        {
            _element = element;
            
        }

        public bool IsEnabled()
        {
            IWebElement element = _driver.FindElement(_locator);
            return element.Enabled;
        }

    }
}
