﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Elements
{
    public class Dropdown : Element
    {
        public Button SelectENLanguage { get; }
        public Button SelectUALanguage { get; }
        public TextInput DirectionSearchField { get; }
        public ListItem AllCountriesItem { get; }


        public Dropdown(IWebDriver driver, By locator) : base(driver, locator)
        {
            SelectENLanguage = new Button(_driver, By.CssSelector("div.active > div:nth-child(2) > button:nth-child(1)"));
            SelectUALanguage = new Button(_driver, By.CssSelector("div.active > div:nth-child(2) > button:nth-child(3)"));
            DirectionSearchField = new TextInput(driver, By.CssSelector("div.create-article__route-input:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1) > label:nth-child(1) > input:nth-child(2)"));
            AllCountriesItem = new ListItem(driver, By.CssSelector("div.create-article__route-input:nth-child(2) > div:nth-child(1) > div:nth-child(3) > div:nth-child(1) > div:nth-child(1) > ul:nth-child(2) > li"));
        }
        
        
    }

}
