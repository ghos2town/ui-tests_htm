﻿using HTM.Driver;
using HTM.Drivers;
using HTM.Pages;
using NLog;
using NLog.Config;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Tests
{
    [AllureNUnit]
    [AllureSuite("Login Feature")]
    [AllureFeature("Login Feature")]
    [AllureEpic("HTM website")]
    

    public class LoginTests : BaseTest
    {

        [Test(Description = "LoginPasswordAreValid")]
        [AllureStory("LoginPasswordAreValid")]
        [AllureStep("LoginPasswordAreValid")]
        [AllureTag("Smoke")]
        public void LoginPasswordAreValid()
        {  
            try
            {
                // Arrange
                string expectedText = "Ваше направление";


                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.EnterButton.Click();
                Console.WriteLine("Navigated to HTM website and clicked on Enter button on Main page");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                // Login via email
                var loginpage = new LoginPage(driver);
                loginpage.Login("anniebrusentsova@gmail.com", "qwerty4321");

                string actualText = mainpage.GetDirectionText();


                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("Login Case Validated Successfully");
            }

            catch (Exception ex)
            {
                logger.Error("Login Failed: " + ex);
                Assert.Fail();
            }
           
        }

        [Test(Description = "PasswordIsInvalid")]
        [AllureStory("PasswordIsInvalid")]
        [AllureStep("PasswordIsInvalid")]
        [AllureTag("Smoke")]
        public void PasswordIsInvalid()
        {
            try
            {
                // Arrange
                string expectedText = "Пароль должен быть больше 8 символов";

                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.EnterButton.Click();
                Console.WriteLine("Navigated to HTM website and clicked on Enter button on Main page");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                // Login via email
                var loginpage = new LoginPage(driver);
                loginpage.Login("anniebrusentsova@gmail.com", "1111");

                string actualText = loginpage.GetPasswordErrorText();

                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'PasswordIsInvalid' Case Validated Successfully");
            }

            catch (Exception ex)
            {
                logger.Error("'PasswordIsInvalid' Test Failed: " + ex);
                Assert.Fail();
            }


        }


        [Test(Description = "EmailIsInvalid")]
        [AllureStory("EmailIsInvalid")]
        [AllureStep("EmailIsInvalid")]
        [AllureTag("Smoke")]
        public void EmailIsInvalid()
        {
            try
            {
                // Arrange
                string expectedText = "К сожалению, регистрация новых пользователей временна остановлена. Если у вас есть вопросы, обратитесь в тех.поддержку.";

                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.EnterButton.Click();
                Console.WriteLine("Navigated to HTM website and clicked on Enter button on Main page");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                // Login via email
                var loginpage = new LoginPage(driver);
                loginpage.LoginWIthInvalidEmail("aanniebrusentsova@gmail.com");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

                string actualText = loginpage.GetLoginText();

                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'EmailIsInvalid' Case Validated Successfully");
            }

            catch (Exception ex)
            {
                logger.Error("'EmailIsInvalid' Test Failed: " + ex);
                Assert.Fail();
            }


        }


    }

    
}
