﻿using HTM.Drivers;
using HTM.Pages;
using NLog;
using NLog.Config;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Tests
{
    
    [AllureNUnit]
    [AllureSuite("Localization Testing")]
    [AllureFeature("Localization Testing")]
    [AllureEpic("HTM website")]
    public class LocalizationTests : BaseTest
    {
        
        [Test(Description = "CheckRUSVersion")]
        [AllureStory("CheckRUSVersion")]
        [AllureStep("CheckRUSVersion")]
        [AllureTag("Smoke")]
        public void CheckRUSVersion()
        {
            try
            {
                // Arrange
                string expectedText = "Ваше направление";

                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                logger.Info("StartButton Clicked Successfully");
                Console.WriteLine("Navigated to HTM website");

                var mainpage = new MainPage(driver);
                string actualText = mainpage.GetDirectionText();

                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'CheckRUSVersion' Case Validated Successfully");
            }

            catch (Exception ex)
            {
                logger.Error("'CheckRUSVersion' Test Failed: " + ex);
                Assert.Fail();
            }
        }


        [Test(Description = "CheckENVersion")]
        [AllureStory("CheckENVersion")]
        [AllureStep("CheckENVersion")]
        [AllureTag("Smoke")]
        public void CheckENVersion()
        {
            try
            {
                // Arrange
                string expectedText = "Your direction";

                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.LanguageDropdown.Click();

                mainpage.Header.LanguageDropdown.SelectENLanguage.Click();
                logger.Info("Language Changed Successfully");
                Console.WriteLine("Language is changed");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                string actualText = mainpage.GetDirectionText();

                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'CheckENVersion' Case Validated Successfully");
            }
           

            catch (Exception ex)
            {
                logger.Error("'CheckENVersion' Test Failed: " + ex);
                Assert.Fail();
            }

        }

        [Test(Description = "CheckUAVersion")]
        [AllureStory("CheckUAVersion")]
        [AllureStep("CheckUAVersion")]
        [AllureTag("Smoke")]
        public void CheckUAVersion()
        {
            try
            {
                // Arrange
                string expectedText = "Ваш напрямок";

                // Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.LanguageDropdown.Click();

                mainpage.Header.LanguageDropdown.SelectUALanguage.Click();
                logger.Info("Language changed Successfully");
                Console.WriteLine("Language is changed");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                string actualText = mainpage.GetDirectionText();

                // Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'CheckUAVersion' Case Validated Successfully");
            }
           

            catch (Exception ex)
            {
                logger.Error("'CheckUAVersion' Test Failed: " + ex);
                Assert.Fail();
            }

        }

       

    }

    
}
