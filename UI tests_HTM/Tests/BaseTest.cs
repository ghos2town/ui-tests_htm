﻿using HTM.Driver;
using HTM.Pages;
using NLog;
using NLog.Config;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HTM.Drivers
{
    public class BaseTest
    {
        public IWebDriver driver;
        public static ILogger logger = LogManager.GetCurrentClassLogger();      

        [SetUp]
        public void Setup()
        {
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.config");
            var driverurl = new DriverUrl();         
            driver = new FirefoxDriver(driverurl.Url); 
            logger.Info("Driver Initialized Successfully");
            var promopage = new PromoPage(driver);
            driver.Navigate().GoToUrl(promopage.Url);   
            Console.WriteLine("Navigated to promo website");
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            logger.Info("Promo Url Opens Successfully");
        }


        [TearDown]
        public void CloseBrowser()
        {
            driver?.Quit();

        }

        [OneTimeTearDown]
        public async Task GenerateAllureReportAsync()
        {
            ProcessStartInfo startInfo = new ProcessStartInfo
            {
                FileName = "cmd",
                RedirectStandardInput = true,
                UseShellExecute = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true
            };

            using (Process process = new Process { StartInfo = startInfo })
            {
                process.Start();
                process.StandardInput.WriteLine("allure serve allure-results");
                process.StandardInput.Close();
                await process.WaitForExitAsync(); 
                process.StandardInput.WriteLine("allure serve --stop");
            }
        }
    }
}
