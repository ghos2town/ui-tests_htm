﻿using HTM.Driver;
using HTM.Drivers;
using HTM.Pages;
using NLog;
using NLog.Config;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Tests
{
    
    [AllureNUnit]
    [AllureSuite("CreateArticle Feature")]
    [AllureFeature("CreateArticle Feature")]
    [AllureEpic("HTM website")]
    public class CreateArticleTests : BaseTest
    {

        [Test(Description = "ArticleCreation")]
        [AllureStory("ArticleCreation")]
        [AllureStep("ArticleCreation")]
        [AllureTag("Smoke")]
        public void ArticleCreation()
        {
            try
            {
                
                //Arrange
                string expectedText = "Ваши статьи";

                //Act
                var promopage = new PromoPage(driver);
                promopage.PromoHeader.StartButton.Click();
                Console.WriteLine("Start button is clicked on Promopage");

                var mainpage = new MainPage(driver);
                mainpage.Header.EnterButton.Click();
                Console.WriteLine("Navigated to HTM website and clicked on Enter button on Main page");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(70);

                // Login via email
                var loginpage = new LoginPage(driver);
                loginpage.Login("anniebrusentsova@gmail.com", "qwerty4321");

                // Click on the 'Create an article' button
                var profilepage = new ProfilePage(driver);
                profilepage.CreateArticleButton.Click();
                logger.Info("'Create an article' Button Clicked Successfully");
                Console.WriteLine("'Create an article' button is clicked");

                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);

                // Select tags
                var articlepage = new ArticlePage(driver);
                articlepage.TagButton.Click();

                articlepage.MentalityTag.Click();
                articlepage.TagCloseButton.Click();
                Console.WriteLine("Tag is selected");

                // Select the direction
                articlepage.DirectionButton.Click();

                //Select "To*"
                articlepage.CountryToDropdown.Click();

                //Enter Country in the search field
                articlepage.CountryToDropdown.DirectionSearchField.EnterText("Все страны");

                //Select the item (country)
                articlepage.CountryToDropdown.AllCountriesItem.Click();

                //Click on the Apply button
                articlepage.ApplyButton.Click();
                Console.WriteLine("Direction is selected");

                // Enter an article title
                articlepage.ArticleTitle.EnterText("Test11");
                Console.WriteLine("Article title is entered");

                //Publish the article       
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
              
                bool isPublishButtonEnabled = articlepage.PublishButton.IsEnabled();
                articlepage.PublishButton.Click();

                logger.Info("Article Created Successfully");
                Console.WriteLine("Article is created");

                string actualText = profilepage.GetArticleText();


                //Assert
                Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedText, actualText);
                logger.Info("'ArticleCreation' Case Validated Successfully");
            }
            

            catch (Exception ex)
            {
                logger.Error("'ArticleCreation' Test Failed: " + ex);
                Assert.Fail();
            }
        }

        
    }

    
}
