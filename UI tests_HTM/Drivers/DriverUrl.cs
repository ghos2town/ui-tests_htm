﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Driver
{
    public class DriverUrl
    {
        public string Url { get; set; }

        public DriverUrl()
        {
            Url = "D:/Drivers/geckodriver"; //https://github.com/mozilla/geckodriver/releases/
        }
    }
}
