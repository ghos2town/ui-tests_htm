﻿using HTM.Elements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Pages
{
    public class ArticlePage : Page
    {
        public Button TagButton { get; }
        public Button MentalityTag { get; }
        public Button TagCloseButton { get; }
        public Button DirectionButton { get; }
        public Dropdown CountryToDropdown { get; }
        public Button ApplyButton { get; }
        public TextInput ArticleTitle { get; }
        public Button PublishButton { get; }


        public ArticlePage(IWebDriver driver) : base(driver)
        {
            TagButton = new Button(driver, By.XPath("/html/body/div[1]/main/div[1]/div[1]/div[2]/div/div[1]/div[2]/button/span"));
            MentalityTag = new Button(driver, By.XPath("/html/body/div[2]/div[4]/div/div[2]/div/div[1]/ul/li[1]"));
            TagCloseButton = new Button(driver, By.XPath("/html/body/div[2]/div[4]/div/div[1]/button"));
            DirectionButton = new Button(driver, By.XPath("/html/body/div[1]/main/div[1]/div[1]/div[2]/div/div[1]/div[1]/button/span"));
            CountryToDropdown = new Dropdown(driver, By.CssSelector("div.create-article__route-input:nth-child(2) > div:nth-child(1)"));
            ApplyButton = new Button(driver, By.CssSelector("button.size-button-sm"));
            ArticleTitle = new TextInput(driver, By.XPath("//textarea[contains(@class, 'el-textarea__inner')]"));
            PublishButton = new Button(driver, By.CssSelector(".create-article-sidebar__button-save"));
        }


    }
}
