﻿using HTM.Elements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Pages
{
    public class ProfilePage : Page
    {
        public Button CreateArticleButton { get; }

        public ProfilePage(IWebDriver driver) : base(driver)
        {
            CreateArticleButton = new Button(driver, By.XPath("/html/body/div[1]/main/div[1]/div/div[2]/div/div[1]/div[2]/div[2]/a[1]"));
        }
        
        public IWebElement GetArticleTextElement() => _driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[2]/div/div[2]/div[1]/div/button[1]"));



        public string GetArticleText()
        {
            return GetArticleTextElement().Text;
        }
    }
}
