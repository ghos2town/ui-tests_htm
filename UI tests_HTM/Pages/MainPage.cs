﻿using HTM.Elements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Pages
{
    public class MainPage : Page
    {
        public Header Header { get; }


        public MainPage(IWebDriver driver) : base(driver)
        {
            Header = new Header(driver, By.XPath("/html/body/div[1]/main/header/div/div"));
        }



        public IWebElement GetDirectionTextElement() => _driver.FindElement(By.XPath("/html/body/div[1]/main/div[1]/div/div[1]/div[2]/div[1]/h3"));


        public string GetDirectionText()
        {
            return GetDirectionTextElement().Text;
        }

    }
}
