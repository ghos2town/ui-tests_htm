﻿using HTM.Tests;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Pages
{
    public class Page
    {
        public IWebDriver _driver;

        public Page(IWebDriver driver)
        {
            _driver = driver;
        }

        public string Name { get; set; }
        public string Url { get; set; }
    }
}
