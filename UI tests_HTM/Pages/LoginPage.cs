﻿using HTM.Elements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;

namespace HTM.Pages
{
    public class LoginPage : Page
    {
        public static ILogger logger = LogManager.GetCurrentClassLogger();

        public TextInput EmailInput { get; }
        public Button ContinueButton { get; }
        public TextInput PasswordInput { get; }
        public Button LoginButton { get; }

        public LoginPage(IWebDriver driver) : base(driver)
        {
            EmailInput = new TextInput(driver, By.XPath("/html/body/div/div/div/form/div/label/input"));
            ContinueButton = new Button(driver, By.XPath("/html/body/div/div/div/form/button"));
            PasswordInput = new TextInput(driver, By.XPath("/html/body/div/div/div/form/div/label/input"));
            LoginButton = new Button(driver, By.XPath("/html/body/div/div/div/form/button[2]"));
        }

        public void Login(string email, string password)
        {
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.config");

            // Login via email
            EmailInput.Clear();
            EmailInput.EnterText(email);
            Console.WriteLine("Email is entered");
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            logger.Info("Email Entered Successfully");
            ContinueButton.Click();
            Console.WriteLine("Continue button is clicked on Login page");

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(100)); // Set explicit wait
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("/html/body/div/div/div/form/button[2]")));

            // Enter password
            PasswordInput.Clear();
            PasswordInput.EnterText(password);
            logger.Info("Password Entered Successfully");
            Console.WriteLine("Password is entered");

            // Enter in the account
            LoginButton.Click();
            Console.WriteLine("Login button is clicked");
        }

        public void LoginWIthInvalidEmail(string email)
        {
            LogManager.Configuration = new XmlLoggingConfiguration("NLog.config");

            // Login via email
            EmailInput.Clear();
            EmailInput.EnterText(email);
            Console.WriteLine("Email is entered");
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(100);
            logger.Info("Email Entered Successfully");
            ContinueButton.Click();
            Console.WriteLine("Continue button is clicked on Login page");

            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(100)); // Set explicit wait
            IWebElement element = wait.Until(ExpectedConditions.ElementExists(By.XPath("//*[@id='app']/div/div/a")));

        }

        public IWebElement GetPasswordErrorTextElement() => _driver.FindElement(By.XPath("/html/body/div/div/div/form/div/p/span"));
        public IWebElement GetLoginTextElement() => _driver.FindElement(By.XPath("/html/body/div[1]/div/div/h2"));


        public string GetPasswordErrorText()
        {
            return GetPasswordErrorTextElement().Text;
        }

        public string GetLoginText()
        {
            return GetLoginTextElement().Text;
        }

    }
}
