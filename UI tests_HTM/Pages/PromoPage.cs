﻿using HTM.Elements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTM.Pages
{
    public class PromoPage : Page
    {
        public PromoHeader PromoHeader { get; }


        public PromoPage(IWebDriver driver) : base(driver)
        {
            Url = "https://howtomove.ilavista.tech/ru/promo";
            PromoHeader = new PromoHeader(driver, By.XPath("/html/body/div/header/div/div[1]"));
        }


    }
}
